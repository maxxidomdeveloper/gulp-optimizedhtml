# Gulp-optimized HTML 5.2 Template for Frontend developers #

> [_Alexander_ _Hold_ ](http://bloghold.de)
>
> [_www.maxxidom.com_ ](http://maxxidom.com)
> [_www.maxxidom.de_ ](http://maxxidom.de)

### OptimizedHTML 5.2 - all-inclusive ###
 **OptimizedHTML** Start Template uses the best practices of web development and optimized for Google PageSpeed.

***
> * Gulp
> * Sass
> * Less
> * Bower
> * htaccess

### Cross-browser ###
***
> _compatibility:_ _IE9+._


### Git push
1. `git init`

2. `git add .`

3. `git commit -m "first commit"`

4. `git remote add origin https:...`

5. `git push -u origin master`


### GULP
>1. gulp: запуск дефолтного gulp таска (sass, js, watch, browserSync) для разработки;
>2. build: сборка проекта в папку dist (очистка, сжатие картинок, удаление всего лишнего);
>3. deploy: выгрузка проекта на рабочий сервер из папки dist по FTP;
>4. clearcache: очистка кеша gulp. Полезно для очистки кеш картинок и закешированных путей.

## Gulp-Config
### Settings

| Переменные      |   свойство    | Описание                    |
| --------------- |-------------- | ------------------------------ |
| prepros         | string        | Препроцессор (SASS, SCSS or LESS).   |
| soursmap        | boolean       | Подключение soursmap    |
| cleanMinCss     | boolean       | сжимать css файла.    |
| buildPress      | boolean       | обединяет все файлы в папке (js,css)  (и удаляет из index.html файлы libs.min.css,main.min.js).     |


### Переменные

    app = app
    css= css
    fonts = fonts
    images = img
    js = js
    libs = libs
    prepros
        = sass
        = less
    htmlfile = index.html
    dist = dist


### Стандартные Размеры h1-h6 в пикселях

    h1 – 32 пикселя.
    h2 – 24 пикселя.
    h3 – 19 пикселям.
    h4 – 16 пикселей.
    h5 – 13 пикселей.
    h6 – 11 пикселей.
    
    
#### Version 1.5.3
>Добавлен scss препроцессор.


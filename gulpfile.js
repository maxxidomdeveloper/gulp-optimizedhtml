/**
 * maxxidom-gulp v1.5.3
 * Autor: Alexander Bechthold
 * Licensed under the MIT license
 */
/*********************************************************************************************************************
                                                    *** Requires ***
**********************************************************************************************************************/

    const gulpConfig   = require('./gulpfile-config'); // Файл конфигурации (gulp-config.js)
    const gulp         = require('gulp');              // сам gulp
    const sourcemaps   = require('gulp-sourcemaps');   // sourcemap для отладки css.
    const autoprefixer = require('gulp-autoprefixer'); // создает префиксы для старых браузеров.
    const cleanCSS     = require('gulp-clean-css');    // сжимает css файл.
    const browserSync  = require('browser-sync');      // Браузер.
    const uglify       = require('gulp-uglify');       // Сжимает js файл.
    const concat       = require('gulp-concat');       // создает файлы.
    const notify       = require("gulp-notify");       // ??? Узнать
    const rename       = require('gulp-rename');       // Меняет название файла.
    const del          = require('del');               // удаляет фаил.
    const cache        = require('gulp-cache');        // очищает cache.
    const imagemin     = require('gulp-imagemin');     // Сжимает картинки.
    const gulpif       = require('gulp-if');           // if
    const rmLines      = require('gulp-rm-lines');     // Удаляет строку в файле.
    const replace      = require('gulp-replace');      // заменяет строку в файле.
    const gcmq         = require('gulp-group-css-media-queries'); // групирует все медиа запросы и скидает в конец.


/*********************************************************************************************************************
                                                    *** Includes || Variables ***
**********************************************************************************************************************/

    const config = gulpConfig.config();

    let html     = config.html;
    let soursmap = config.soursmap;
    let cleancss = config.cleancss;


    let app       = config.app;
    let appFolder = app.folder; // app/

    let dist       = config.dist;
    let distName   = dist.name;
    let distFolder = dist.folder; // dist/

    let src      = config.src;
    let srcFile  = src.file; // *.html

    let images       = config.images;
    let imagesTask   = images.task_name;  // imagemin
    let imagesFolder = images.folder; // img/

    let prepros   = config.prepros;
    let preFolder = prepros.folder;      // prepros/
    let preTask   = prepros.task_name;   // prepros
    let preName   = prepros.outputStyle; // sass or less

    let css           = config.css;
    let cssFolder     = css.folder;             // css/
    let cssConcatTask = css.concat.task_name;   // concat-css
    let cssConcatFile = css.concat.concat_file; // libs.min.css
    let cssMainFile   = css.main.concat_file;   // main.css

    let js           = config.js;
    let jsFolder     = js.folder;             // js/
    let jsMainTask   = js.main.task_name;     // main-js
    let jsConcatTask = js.concat.task_name;   // concat-js
    let jsMainFile   = js.main.concat_file;   // main.js
    let jsConcatFile = js.concat.concat_file; // scripts.min.js

    let fonts       = config.fonts;
    let fontsFolder = fonts.folder; // fonts/

    let browser     = config.browser;
    let browserTask = browser.task_name; // browserSync

    let watch     = config.watch;
    let watchTask = watch.task_name; // watch

    let libs          = config.libs;
    let libsFolder    = libs.folder;
    let libsFontsTask = libs.fonts.task_name;  // font-libs
    let libsTask      = libs.task_name; // libs

    let build        = config.build;
    let buildTask    =  build.task_name;
    let buildPress   = build.press;
    let buildFileCss = build.concat_file.css;
    let buildFileJs  = build.concat_file.js;

    let cssLibsArr = [];
    for(let i = 0; i < css.libs_arr.length; i++) {
        cssLibsArr.push(app.folder + libs.folder + css.libs_arr[i]);
    }

    let jsLibsArr = [];
    for(let i = 0; i < js.libs_arr.length; i++){
        jsLibsArr.push(app.folder + libs.folder + js.libs_arr[i]);
    }


/*********************************************************************************************************************
                                            *** Require and Settings Prepros ***
**********************************************************************************************************************/

    let preprosRequire; // Подключение препроцессора.
    let preprosSrc;     // Поиск файлов препроцессора.

    switch(prepros.outputStyle) {
        case 'sass':
             preprosRequire = require('gulp-sass');
             preprosSrc = '/**/*.';
             break;
        case 'scss':
            preprosRequire = require('gulp-sass');
            preprosSrc = '/**/*.';
            break;
        case 'less':
             preprosRequire = require('gulp-less');
             preprosSrc = '/main.';
             break;
    }

    let preprosFile = ( preprosSrc + prepros.outputStyle ); // Для поиска файла! (src:)


/*********************************************************************************************************************
                                                *** Settings Task ***
**********************************************************************************************************************/

   gulp.task('settings', function(){

       let rexArr = [/libsCss(.{35,})\w/g, /mainCss(.{35,})\w/g, /libsJs(.{0,})\w/g, /mainJs(.{0,})\w/g];
       let index = gulp.src([appFolder +'index.html'])
           .pipe(replace(rexArr[0], function(match) { return reep(cssConcatFile, 'css', match);}))
           .pipe(replace(rexArr[1], function(match) { return reep(cssMainFile, 'css', match); }))
           .pipe(replace(rexArr[2], function(match) { return reep(jsConcatFile, 'js', match); }))
           .pipe(replace(rexArr[3], function(match) { return reep(jsMainFile, 'js', match); }))
           .pipe(gulp.dest(appFolder));

   });

/*********************************************************************************************************************
                                                *** Gulp TASK ***
**********************************************************************************************************************/

    /***/////////////////  PREPROS ( SASS|SCSS|LESS )   /////////////////***/
    gulp.task(preTask, function(){

        return gulp.src(appFolder + preFolder + preName + preprosFile) // Вызывается в самом начале. Поиск файла.
            .pipe(gulpif(soursmap, sourcemaps.init())) // Создание soursmap.
                .pipe(preprosRequire({outputStyle: 'expand'}).on("error", notify.onError())) // Препорцессор.
                .pipe(gcmq()) // группирует Медиа запроссы.
                .pipe(autoprefixer({ browsers: ['> 0.1%'], cascade: false })) // Ставит превиксы.
            .pipe(gulpif(soursmap, sourcemaps.write('.'))) // запись файла soursmap.
            .pipe(gulp.dest(appFolder + cssFolder)) // путь куда будет записан фаил. (app/css)
            .pipe(browserSync.reload({stream: true})); //перзагруска браузера.

    });

    /***/////////////////  Concat-CSS   /////////////////***/
    gulp.task(cssConcatTask, [preTask], function(){

        return gulp.src(cssLibsArr)
            .pipe(concat(cssConcatFile))
            .pipe(cleanCSS())
            .pipe(gulp.dest(appFolder + cssFolder))
            .pipe(browserSync.reload({stream: true}));

    });

    /***/////////////////  Main-Js   /////////////////***/
    gulp.task(jsMainTask, function() {
        return gulp.src([appFolder + jsFolder + jsMainFile])
            .pipe(browserSync.reload({stream: true}));
    });

    /***/////////////////  Concat-Js   /////////////////***/
    gulp.task(jsConcatTask, [jsMainTask], function() {

        return gulp.src(jsLibsArr)
            .pipe(concat(jsConcatFile)) // объединяет все скрипты и создает один фаил.
            .pipe(uglify()) // Минимизировать весь js
            .pipe(gulp.dest(appFolder + jsFolder)) // путь куда будет записан фаил.
            .pipe(browserSync.reload({stream: true}));

    });

    /***/////////////////  Browser   /////////////////***/
    gulp.task(browser.task_name, function() {
        browserSync({
            server: { baseDir: app.folder },
            notify: false
        });
    });

    /***/////////////////  Watch   /////////////////***/
    gulp.task(watchTask, [preTask, cssConcatTask, jsMainTask, jsConcatTask, browserTask], function() {

        gulp.watch( appFolder + preFolder + preName + '/' + src.all + '.' + preName, [preTask] ); // prepros
        gulp.watch( appFolder + jsFolder + jsMainFile, [jsMainTask]); // main-js
        gulp.watch( appFolder + jsFolder + jsConcatFile, [jsConcatTask]); // concat-js
        gulp.watch( appFolder + src.file, browserSync.reload); // html

    });

    /***/////////////////  DEFAULT GULP   /////////////////***/
    gulp.task('default', [watchTask, libsTask]);


/*********************************************************************************************************************
                                                    *** Libraries ***
**********************************************************************************************************************/

    /***/////////////////  LIBRARIEs   /////////////////***/
    gulp.task(libsTask, [libsFontsTask], function() {

        return c('Библиотека обновилась!!!');

    });

    /***  FONTs  ***/
    gulp.task(libsFontsTask, function(){

        let appLibs = appFolder + libsFolder; // app/libs/
        let folderFonts = fontsFolder + libsTask; //'fonts/libs'
        let fontname;

        gulp.src([appLibs + src.css])
            .pipe(replace(/fonts(.{2})\w*/g, function(match) {
                fontname = match.substr(6);
                if(fontname === libsTask){ return folderFonts; }
                return folderFonts + '/' + fontname;
            }))
            .pipe(gulp.dest(appLibs));

        gulp.src([ appFolder + libsFolder + '**/+(fonts|font)/**/*'])
            .pipe(rename(function(path) { path.dirname = '.'; }))
            .pipe(gulp.dest(appFolder + fontsFolder+ libsFolder)); // app/fonts/libs/

    });

/*********************************************************************************************************************
                                            *** Concate Project (dist) ***
**********************************************************************************************************************/


    gulp.task(buildTask, ['removedist'], function() {

        // Files
        let buildFiles = gulp.src([ appFolder + srcFile])
            .pipe(gulpif(buildPress, rmLines({'filters': [ /mainCss/i, /mainJs/i ]})))
            .pipe(gulpif(buildPress, replace(/libsCss(.{35,})\w/g, function(match) { return reep(buildFileCss, 'css', match);})))
            .pipe(gulpif(buildPress, replace(/libsJs(.{0,})\w/g, function(match) { return reep(buildFileJs, 'js', match);})))
            .pipe(gulp.dest(distFolder))
            .on('end', ()=> { c('BuildFiles = OK!'); });


        // .htaccess
        let htaccess = gulp.src(appFolder + '/ht.access')
            .pipe(concat('.htaccess'))
            .pipe(gulp.dest(distFolder))
            .on('end', ()=> { c('BuildHtaccess = OK!'); });

        // CSS
        let buildCss = gulp.src([
            appFolder + cssFolder + cssConcatFile,
            appFolder + cssFolder + cssMainFile
        ])
            .pipe(gulpif(buildPress, concat(buildFileCss)))
            .pipe(gulpif(buildPress, cleanCSS()))
            .pipe(gulp.dest(distFolder + cssFolder))
            .on('end', ()=> { c('BuildCSS = OK!'); });

        // Js
        let buildJs = gulp.src([
            appFolder + jsFolder + jsConcatFile,
            appFolder + jsFolder + jsMainFile
        ])
            .pipe(gulpif(buildPress, concat(buildFileJs)))
            .pipe(gulpif(buildPress, uglify()))
            .pipe(gulp.dest(distFolder + jsFolder))
            .on('end', ()=> { c('BuildJs = OK!'); });

        // fonts
        let buildFonts = gulp.src([
            appFolder + fontsFolder + src.all
        ])
            .pipe(gulp.dest(distFolder + fontsFolder))
            .on('end', ()=> { c('BuildFonts = OK!'); });

    });



/*********************************************************************************************************************
                                                *** FUNCTIONs and SubTask ***
**********************************************************************************************************************/

    /***/////////////////  Min-Images   /////////////////***/
    gulp.task(imagesTask, function() {
        return gulp.src(appFolder + imagesFolder + src.all)
        .pipe(cache(imagemin()))
        .pipe(gulp.dest(distFolder + imagesFolder));
    });

    /***/////////////////  Folder remove   /////////////////***/
    gulp.task('removedist', function() {
        del.sync(distFolder);
        return c('Folder: [' + distName + '] successfully deleted!');
    });

    /***/////////////////  Clearcache   /////////////////***/
    gulp.task('clearcache', function () { return cache.clearAll(); });


    /***/////////////////  в html изменение (css,js) файлов.   /////////////////***/
    gulp.task('clearcache', function () { return cache.clearAll(); });
    function reep(style, t, match) {

        let classElement;
        let hrefElement;
        let erg;
        let def = true;

        switch(t) {
            case 'css':
                classElement = match.substring(0,7);
                hrefElement  = match.substring(36);
                erg = classElement + '" rel="stylesheet" href="' + cssFolder + style + '';
                break;
            case 'js':
                classElement = match.substring(0,6);
                hrefElement  = match.substring(16);
                erg = classElement + '" src="' + jsFolder + style + '"></script';
                break;
            default:
                c('Неизвестное значение: ' + t + '. разрешенные значения: (css, js)');
                def = false;
        }

        if(def){ return erg; }

    }

    /***/////////////////  вывод в консоль!   /////////////////***/
    function c(str) {
        console.log(str);
    }
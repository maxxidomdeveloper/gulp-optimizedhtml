/**
 * maxxidom-gulp v1.5.3
 * Autor: Alexander Bechthold
 * Licensed under the MIT license
 */
/*********************************************************************************************************************
                                                *** Settings ***
**********************************************************************************************************************/
    const settings = {
        soursMap: false,
        cleanCss: false,
        buildPress: true,
        outputStyle: 'sass',
        mainFolders: {
            app: 'app',
            dist: 'dist'
        },
        subFolders: {
            css: {
                folder: 'css',
                file: {
                    main: 'main.css',
                    libs: 'libs.min.css'
                }
            },
            fonts: 'fonts',
            images: 'img',
            js: {
                folder: 'js',
                file: {
                    main: 'main.js',
                    concat: 'scripts.min.js'
                },
            },
            libs: 'libs',
            prepros: 'prepros'
        },
        build: {
            press_file_css: 'concat.css',
            press_file_js: 'concat.js'
        },

        index: 'html'
    };

/*********************************************************************************************************************
                                                *** Libraries ***
             !!! При добавлении в библиотеку ее нужно обязательно инициализировать. [ gulp libs ] !!!
**********************************************************************************************************************/

    /*** все подключаемые библиотки js. ***/
    let jsLibrariesArr = [
        'jquery/dist/jquery.min.js', // jQuery
        //'bootstrap/dist/js/bootstrap.min.js' // Bootstrap js
    ];

    /*** все подключаемые библиотки css. ***/
    let cssLibrariesArr = [
        //'bootstrap-grid/bootstrap-grid.css', // Bootstrap сетка
        //'bootstrap/dist/css/bootstrap.min.css', // Bootstrap css
        'font-awesome/css/font-awesome.min.css' // Иконочный шрифт.
    ];

    /*** все подключаемые библиотки картинки. ***/
    let imagesLibrariesArr = [];



/*********************************************************************************************************************
                                                *** Configurations ***
**********************************************************************************************************************/

    let subFolders  = settings.subFolders;
    let mainFolders = settings.mainFolders;

    const config = {
        app: {
            folder: mainFolders.app + '/'
        },
        dist: {
            name: mainFolders.dist,
            folder: mainFolders.dist + '/'
        },
        src: {
            all: '**/*',
            js: '**/*.js',
            css: '**/*.css',
            file: '*.' + settings.index
        },
        html: {
            name: 'html'
        },
        css: {
            folder: subFolders.css.folder + '/',
            main: {
                concat_file: subFolders.css.file.main,
                concat_File_default: 'main.css'
            },
            concat: {
                task_name: 'concat-css',
                concat_file: subFolders.css.file.libs,
                concat_File_default: 'libs.min.css'
            },
            libs_arr: cssLibrariesArr
        },
        fonts: {
            folder: subFolders.fonts + '/'
        },
        images: {
            task_name: 'imagemin',
            folder: subFolders.images + '/'
        },
        js: {
            folder: subFolders.js.folder + '/',
            main: {
                task_name: 'main-js',
                concat_file: subFolders.js.file.main
            },
            concat: {
                task_name: 'concat-js',
                concat_file: subFolders.js.file.concat
            },
            libs_arr: jsLibrariesArr
        },
        libs: {
            task_name: 'libs',
            folder: subFolders.libs + '/',
            fonts: {
                task_name: 'font-libs'
            }
        },
        prepros: {
            task_name: 'prepros',
            folder: subFolders.prepros + '/',
            outputStyle: settings.outputStyle
        },
        watch: {
            task_name: 'watch',
        },
        browser: {
            task_name: 'browserSync'
        },
        build: {
            task_name: 'build',
            press: settings.buildPress,
            concat_file:  {
                css: settings.build.press_file_css,
                js: settings.build.press_file_js
            }
        },
        soursmap: settings.soursMap,
        cleancss: settings.cleanCss
    };




/*********************************************************************************************************************
                                                *** Export ***
**********************************************************************************************************************/

    exports.settings = function(){return settings;};
    exports.config = function(){return config;};
